let express = require('express');
let bodyParser = require('body-parser');

let app = express();

let db = require('./db');

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/views/pages/static'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/home');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
