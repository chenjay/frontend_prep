class MockDB {
  constructor() {
    this.data = {};
  }

  increment_note_count(note) {
    this.data[note] = (this.data[note] || 0) + 1;
  }
}

module.exports = new MockDB();
